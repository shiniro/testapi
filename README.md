# TestAPI

## Install MongoDB
``` bash
#On mac

    Install MongoDB on the computer : brew install mongodb

    Create a "db" directory (this is where the Mongo data files will live) : mkdir -p /data/db

    Give the right permission to this directory : sudo chown -R `id -un` /data/db

    Run mongo deamon (start the mongo server) : mongod

    Run the mongo shell (with the Mongo daemon running in one terminal - This will run the Mongo shell which is an application to access data in MongoDB) : mongo 

    To exit the Mongo shell: quit()

    To stop the Mongo daemon: ctr + c

```

